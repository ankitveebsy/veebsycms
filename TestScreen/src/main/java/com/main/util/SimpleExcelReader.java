package com.main.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.main.db.CorporateHandler;
import com.main.db.UserDataHandler;
import com.main.model.CorporatePojo;
import com.main.model.UserDetails;

public class SimpleExcelReader {

	public String read(String excelFilePath) throws IOException {

		try{
			System.out.println("File: "+excelFilePath);
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();

			int rowCount = 0;
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				if(rowCount!=0){
					int columnCount = 0;
					UserDetails userDetails = new UserDetails();
					userDetails.setUserType("Corpoprate");
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						switch (columnCount) {
						case 0:
							userDetails.setName(cell.getStringCellValue());
							break;
						case 1:
							userDetails.setEmailID(cell.getStringCellValue());
							break;
						case 2:
							CorporateHandler corporateHandler = new CorporateHandler();
							CorporatePojo corporatePojo = corporateHandler.getCorporate(cell.getStringCellValue());
							if(corporatePojo!=null){
								userDetails.setCorporateName(corporatePojo.getCompanyName());
								userDetails.setSubscriptionPackageName(corporatePojo.getPackageName());
								userDetails.setSubscriptionStartDate(corporatePojo.getSubscriptionStart());
								userDetails.setSubscriptionEndDate(corporatePojo.getSubscriptionEnd());
							}else{
								return "Corporate name is invalid.";
							}
							
							break;
						case 3:
							userDetails.setMob(cell.getStringCellValue());
							break;
						}

						columnCount++;
					}
					UserDataHandler userDataHandler = new UserDataHandler();
					userDataHandler.addNewUser(userDetails);
				}

				rowCount++;
			}

			workbook.close();
			inputStream.close();
			
		}catch(Exception e){
			e.printStackTrace();
			return "Error while uploading excel.";
		}
		
		return "Success";
	}

	public static void downloadExcel(ServletOutputStream out) {

		try {

			org.apache.poi.hssf.usermodel.HSSFWorkbook wb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
			org.apache.poi.hssf.usermodel.HSSFSheet sheet = wb.createSheet("new sheet");
			// Create a row and put some cells in it. Rows are 0 based.
			org.apache.poi.hssf.usermodel.HSSFRow row = sheet.createRow((short) 0);
			// Create a cell and put a value in it.
			org.apache.poi.hssf.usermodel.HSSFCell cell = row.createCell((short) 0);
			cell.setCellValue(1);
			// Or do it on one line.
			row.createCell((short) 1).setCellValue(1.2);
			row.createCell((short) 2).setCellValue("This is a string");
			row.createCell((short) 3).setCellValue(true);
			// Write the output to a file
			// java.io.FileOutputStream fileOut = new java.io.FileOutputStream(
			// "ex3.xls");
			wb.write(out);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}