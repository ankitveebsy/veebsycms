package com.main.spring;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.main.db.CorporateHandler;
import com.main.db.PackageDataHandler;
import com.main.db.UserDataHandler;
import com.main.model.CorporatePojo;
import com.main.model.PackagePojo;
import com.main.model.UserDetails;

@Controller
public class PackageController {

	@RequestMapping(value = "/package", method = RequestMethod.GET)
	public String packageMaster(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("searchCorporate");
		if (req.getSession().getAttribute("user_id") != null) {
			PackageDataHandler p = new PackageDataHandler();
			List<PackagePojo> list = p.getPackageData(null, false);
			req.setAttribute("user", list);
			return "package-master";
		}
		return "login";
	}

	@RequestMapping(value = "/newPackage", method = RequestMethod.POST)
	public String newPackage(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("New user");
		if (req.getSession().getAttribute("user_id") != null) {
			return "new-package";
		}
		return "login";
	}

	@RequestMapping(value = "/addPackage", method = RequestMethod.POST)
	public void addPackage(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("New Package");
		if (req.getSession().getAttribute("user_id") != null) {
			if (req.getParameter("packageName") != null) {
				PackageDataHandler p = new PackageDataHandler();

				PackagePojo pojo = new PackagePojo();
				pojo.setPackageCost("" + req.getParameter("packageCost"));
				pojo.setPackageDuration("" + req.getParameter("packageDuration"));
				pojo.setPackageName("" + req.getParameter("packageName"));

				p.addPackage(pojo);
			}

			try {
				res.sendRedirect("package");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@RequestMapping(value = "/searchPackage", method = RequestMethod.POST)
	public String searchPackage(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("searchCorporate");
		if (req.getSession().getAttribute("user_id") != null) {
			PackageDataHandler p = new PackageDataHandler();
			List<PackagePojo> list = p.getPackageData(req.getParameter("packageToSearch"), true);
			req.setAttribute("user", list);
			return "package-master";
		}
		return "login";
	}

	@RequestMapping(value = "/editPackage", method = RequestMethod.GET)
	public String editUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			if (req.getSession().getAttribute("user_id") != null) {
				PackageDataHandler packageDataHandler = new PackageDataHandler();
				PackagePojo pojo = packageDataHandler.getPackage(req.getParameter("packageName"));
				req.setAttribute("package", pojo);
				return "edit-package";

			} 
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "login";
	}

	@RequestMapping(value = "/removePackage", method = RequestMethod.GET)
	public String removeUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			if (req.getSession().getAttribute("user_id") != null) {
				PackageDataHandler packageDataHandler = new PackageDataHandler();
				boolean b = packageDataHandler.removePackage(req.getParameter("packageName"));
				if(b){
					req.setAttribute("message", null);
				}else{
					req.setAttribute("message", req.getParameter("packageName")+" is in use.");
				}
				PackageDataHandler p = new PackageDataHandler();
				List<PackagePojo> list = p.getPackageData(null, false);
				req.setAttribute("user", list);
				return "package-master";
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "login";
	}

	
	@RequestMapping(value = "/editPackageData", method = RequestMethod.POST)
	public void editUserData(HttpServletRequest req, HttpServletResponse res) {
		if (req.getSession().getAttribute("user_id") != null) {
			if (req.getParameter("packageName") != null) {
				PackageDataHandler p = new PackageDataHandler();

				PackagePojo pojo = new PackagePojo();
				pojo.setPackageCost("" + req.getParameter("packageCost"));
				pojo.setPackageDuration("" + req.getParameter("packageDuration"));
				pojo.setPackageName("" + req.getParameter("packageName"));

				p.editPackage(pojo);
			}

			try {
				res.sendRedirect("package");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
