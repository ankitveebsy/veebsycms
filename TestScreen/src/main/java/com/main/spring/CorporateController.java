package com.main.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.main.db.CorporateHandler;
import com.main.db.PackageDataHandler;
import com.main.model.CorporatePojo;
import com.main.model.PackagePojo;

@Controller
public class CorporateController {

	@RequestMapping(value = "/newCorporate", method = RequestMethod.POST)
	public String newCorporate(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("New Corporate session: " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			PackageDataHandler p = new PackageDataHandler();
			List<PackagePojo> list = p.getPackageData("", false);

			req.setAttribute("packageList", list);
			return "new-corporate";
		}
		return "login";
	}

	@RequestMapping(value = "/addCorporate", method = RequestMethod.POST)
	public void addCorporate(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Add corporate : " + req.getSession().getAttribute("user_id"));

		if (req.getSession().getAttribute("user_id") != null) {
			CorporatePojo pojo = new CorporatePojo();
			pojo.setCompanyName(req.getParameter("company"));
			pojo.setTotalUser(req.getParameter("totalUser"));
			pojo.setPackageName(req.getParameter("package"));
			pojo.setSubscriptionStart(req.getParameter("startDate"));

			CorporateHandler corporateHandler = new CorporateHandler();
			corporateHandler.addCorporate(pojo);

			try {
				res.sendRedirect("corporate");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@RequestMapping(value = "/corporate", method = RequestMethod.GET)
	public String corporate(HttpServletRequest req, HttpServletResponse res) {

		System.out.println("Corporate : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			CorporateHandler corporateHandler = new CorporateHandler();
			List<CorporatePojo> list = corporateHandler.getCorporateData(null, false);

			req.setAttribute("corporateList", list);
			return "corporate-master";
		} else {
			
		}
		// List<CorporatePojo> getCorporateData
		return "login";
	}

	@RequestMapping(value = "/searchCorporate", method = RequestMethod.POST)
	public String searchCorportate(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("search corporate : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			CorporateHandler corporateHandler = new CorporateHandler();
			List<CorporatePojo> list = corporateHandler.getCorporateData(req.getParameter("corporateName"), true);

			req.setAttribute("corporateList", list);
			return "corporate-master";
		}
		return "login";
	}

	@RequestMapping(value = "/editCorporate", method = RequestMethod.GET)
	public String editUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("edit corporate : " + req.getSession().getAttribute("user_id"));
			if (req.getSession().getAttribute("user_id") != null) {
				
				CorporateHandler corporateHandler = new CorporateHandler();
				CorporatePojo pojo = corporateHandler.getCorporate(req.getParameter("corporateName"));
				req.setAttribute("corporate", pojo);
				PackageDataHandler p = new PackageDataHandler();
				List<PackagePojo> list = p.getPackageData("", false);

				req.setAttribute("packageList", list);

				return "edit-corporate";
			} else {
			
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "login";
	}

	@RequestMapping(value = "/editCorporateData", method = RequestMethod.POST)
	public void editUserData(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Edit corporate data : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			CorporatePojo pojo = new CorporatePojo();
			pojo.setCompanyName(req.getParameter("company"));
			pojo.setCountryName(req.getParameter("country"));
			pojo.setTotalUser(req.getParameter("totalUser"));
			pojo.setPackageName(req.getParameter("package"));
			pojo.setSubscriptionStart(req.getParameter("startDate"));

			CorporateHandler corporateHandler = new CorporateHandler();
			corporateHandler.editCorporate(pojo);

			try {
				res.sendRedirect("corporate");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
