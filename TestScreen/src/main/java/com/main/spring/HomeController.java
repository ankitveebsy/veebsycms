package com.main.spring;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.main.db.UserDataHandler;
import com.main.model.UserDetails;
import com.main.util.SimpleExcelReader;

@Controller
public class HomeController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Login UserID: " + req.getSession().getAttribute("user_id"));
		req.getSession().removeAttribute("user_id");
		System.out.println("UserID 2: " + req.getSession().getAttribute("user_id"));

		return "login";

	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.POST)
	public String loginValidator(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Login Page Requested");

		System.out.println(req.getParameter("emailID"));
		System.out.println(req.getParameter("password"));

		req.getSession().setAttribute("user_id", "u123");

		return "dashboard";
	}
	
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String loginValidator2(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Login Page Requested");
		if (req.getSession().getAttribute("user_id") != null) {
			System.out.println(req.getParameter("emailID"));
			System.out.println(req.getParameter("password"));

			req.getSession().setAttribute("user_id", "u123");

			return "dashboard";
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutValidator(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Login Page Requested Logout");

		req.getSession().setAttribute("user_id", null);
		return login(req, res);
	}

	
	
	@RequestMapping(value = "/downloadFile", method = RequestMethod.POST)
	public void downloadFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String filename = "/usr/local/apache8/bin/SampleData.xlsx";
//		String filename = "D:/SampleData.xlsx";
		
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ filename + "\"");
 
			FileInputStream fileInputStream = new FileInputStream(filename);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
	}
}