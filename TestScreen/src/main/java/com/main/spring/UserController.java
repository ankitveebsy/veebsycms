package com.main.spring;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.main.db.CorporateHandler;
import com.main.db.MongoDBConnection;
import com.main.db.PackageDataHandler;
import com.main.db.UserDataHandler;
import com.main.model.CorporatePojo;
import com.main.model.PackagePojo;
import com.main.model.UserDetails;
import com.main.util.SimpleExcelReader;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

@Controller
public class UserController {

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String user(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("user : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			UserDataHandler userDataHandler = new UserDataHandler();
			List<UserDetails> list = userDataHandler.getUserList(null,  false);

			req.setAttribute("user", list);
			return "user-master";
		}
		return "login";
	}

	@RequestMapping(value = "/bulkUser", method = RequestMethod.POST)
	public String bulkUser(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("bulk user : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			System.out.println("Message in BUlk:"+req.getAttribute("message"));
			return "bulk-user";
		}
		return "login";
	}

	@RequestMapping(value = "/searchUser", method = RequestMethod.POST)
	public String searchUser(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("search user : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
//			String type = req.getParameter("optionSelected");
			String usr = req.getParameter("userData");

			UserDataHandler userDataHandler = new UserDataHandler();
			List<UserDetails> list = userDataHandler.getUserList(usr,  true);

			req.setAttribute("user", list);
			return "user-master";
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "/";
	}

	@RequestMapping(value = "/newUser", method = RequestMethod.POST)
	public String newUser(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("new user : " + req.getSession().getAttribute("user_id"));
		if (req.getSession().getAttribute("user_id") != null) {
			PackageDataHandler packageDataHandler = new PackageDataHandler();
			List<PackagePojo> databaseArrayList = packageDataHandler.getPackageData(null, false);

			req.setAttribute("databaseArrayList", databaseArrayList);

			CorporateHandler corporateHandler = new CorporateHandler();
			List<CorporatePojo> ar = corporateHandler.getCorporateData(null, false);

			req.setAttribute("companyList", ar);

			PackageDataHandler pp = new PackageDataHandler();
			List<PackagePojo> list = pp.getPackageData(null, false);
			req.setAttribute("packageList", list);

			return "new-user";
		} else {
			try {
				req.setAttribute("login_error", "Please login first");
				res.sendRedirect("logout");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "/";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public void addUser(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("add user : " + req.getSession().getAttribute("user_id"));
		try {
			if (req.getSession().getAttribute("user_id") != null) {
				UserDataHandler userDataHandler = new UserDataHandler();
				boolean isExist = userDataHandler.isEmailAlreadyUsed(req.getParameter("Email"));

				if (!isExist) {
					UserDetails pojo = new UserDetails();
					pojo.setName(req.getParameter("Name"));
					pojo.setEmailID(req.getParameter("Email"));
					pojo.setMob(req.getParameter("Mobile") == null ? "N/A" : req.getParameter("Mobile"));
					pojo.setUserType(req.getParameter("userType"));
					
					System.out.println("type: "+req.getParameter("userType"));
					
					if (req.getParameter("userType").equalsIgnoreCase("individual")) {
						pojo.setCorporateName("N/A");
						pojo.setSubscriptionPackageName(req.getParameter("package"));

						PackageDataHandler packageDataHandler = new PackageDataHandler();
						PackagePojo packagePojo = packageDataHandler.getPackage(req.getParameter("package"));

						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(sdf.parse(req.getParameter("startDate")));
						calendar.add(Calendar.DATE, Integer.parseInt(packagePojo.getPackageDuration()));
						pojo.setSubscriptionStartDate(req.getParameter("startDate"));
						pojo.setSubscriptionEndDate(sdf.format(calendar.getTime()));

					} else {

						System.out.println("Cor: " + req.getParameter("company"));
						CorporateHandler corporateHandler = new CorporateHandler();
						CorporatePojo corporatePojo = corporateHandler.getCorporate(req.getParameter("company"));
						pojo.setCorporateName(req.getParameter("company"));
						pojo.setSubscriptionPackageName(corporatePojo.getPackageName());
						pojo.setSubscriptionStartDate(corporatePojo.getSubscriptionStart());
						pojo.setSubscriptionEndDate(corporatePojo.getSubscriptionEnd());
					}

					UserDataHandler u = new UserDataHandler();
					u.addNewUser(pojo);
					try {
						res.sendRedirect("user");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					req.setAttribute("error", "Email ID already used.");
					req.getRequestDispatcher("newUser").forward(req, res);
				}
			} else {
				try {
					req.setAttribute("login_error", "Please login first");
					res.sendRedirect("logout");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/emailAvl", method = RequestMethod.POST)
	public String isExist(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("email Avl : " + req.getSession().getAttribute("user_id"));
			if (req.getSession().getAttribute("user_id") != null) {
				DB db = MongoDBConnection.getDB();
				if (db != null) {
					String email = req.getParameter("Email");
					DBCollection dbCollection = db.getCollection("user_master");
					BasicDBObject query = new BasicDBObject();
					query.put("email_id", new BasicDBObject("$regex", email).append("$options", "i"));

					System.out.println("Query : " + query.toJson());
					DBCursor dbCursor = dbCollection.find(query);
					if (dbCursor.size() > 0) {
						req.setAttribute("error", "Email ID already used");
						req.getRequestDispatcher("newUser").forward(req, res);
					}
				}
			} else {
				try {
					req.setAttribute("login_error", "Please login first");
					res.sendRedirect("logout");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/";
	}

	@RequestMapping(value = "/editUser", method = RequestMethod.GET)
	public String editUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("edit user : " + req.getSession().getAttribute("user_id"));

			if (req.getSession().getAttribute("user_id") != null) {
				UserDataHandler userDataHandler = new UserDataHandler();
				UserDetails pojo = userDataHandler.getUserData(req.getParameter("email"));
				System.out.println(req.getParameter("email"));
				System.out.println(pojo);
				if (pojo != null) {
					req.setAttribute("user_data", pojo);
					PackageDataHandler packageDataHandler = new PackageDataHandler();
					List<PackagePojo> databaseArrayList = packageDataHandler.getPackageData(null, false);

					req.setAttribute("databaseArrayList", databaseArrayList);

					CorporateHandler corporateHandler = new CorporateHandler();
					List<CorporatePojo> ar = corporateHandler.getCorporateData(null, false);

					req.setAttribute("companyList", ar);

					PackageDataHandler pp = new PackageDataHandler();
					List<PackagePojo> list = pp.getPackageData(null, false);
					req.setAttribute("packageList", list);

					return "edit-user";
				}
			} else {
				try {
					req.setAttribute("login_error", "Please login first");
					res.sendRedirect("logout");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/";
	}
	
	@RequestMapping(value = "/removeUser", method = RequestMethod.GET)
	public void removeUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("remove user : " + req.getSession().getAttribute("user_id"));

			if (req.getSession().getAttribute("user_id") != null) {
				UserDataHandler userDataHandler = new UserDataHandler();
				if(userDataHandler.removeUser(req.getParameter("email"))){
					
					res.sendRedirect("user");
				}
			} else {
				try {
					req.setAttribute("login_error", "Please login first");
					res.sendRedirect("logout");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/editUserData", method = RequestMethod.POST)
	public void editUserData(HttpServletRequest req, HttpServletResponse res) {
		try {
			System.out.println("edit user data : " + req.getSession().getAttribute("user_id"));

			if (req.getSession().getAttribute("user_id") != null) {
				if (req.getSession().getAttribute("user_id") != null) {
					UserDetails pojo = new UserDetails();
					pojo.setName(req.getParameter("Name"));
					pojo.setEmailID(req.getParameter("Email"));
					pojo.setMob(req.getParameter("Mobile") == null ? "N/A" : req.getParameter("Mobile"));
					pojo.setUserType(req.getParameter("userType"));
					if (req.getParameter("userType").equalsIgnoreCase("individual")) {
						pojo.setCorporateName("N/A");
						pojo.setSubscriptionPackageName(req.getParameter("package"));
						System.out.println("Pac: " + req.getParameter("package"));
						PackageDataHandler packageDataHandler = new PackageDataHandler();
						PackagePojo packagePojo = packageDataHandler.getPackage(req.getParameter("package"));

						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(sdf.parse(req.getParameter("startDate")));
						calendar.add(Calendar.DATE, Integer.parseInt(packagePojo.getPackageDuration()));
						pojo.setSubscriptionStartDate(req.getParameter("startDate"));
						pojo.setSubscriptionEndDate(sdf.format(calendar.getTime()));

					} else {
						pojo.setCorporateName(
								req.getParameter("company") == null ? "N/A" : req.getParameter("company"));
						CorporateHandler corporateHandler = new CorporateHandler();
						CorporatePojo corporatePojo = corporateHandler.getCorporate(req.getParameter("company"));
						pojo.setCorporateName(req.getParameter("company"));
						pojo.setSubscriptionPackageName(corporatePojo.getPackageName());
						pojo.setSubscriptionStartDate(corporatePojo.getSubscriptionStart());
						pojo.setSubscriptionEndDate(corporatePojo.getSubscriptionEnd());
					}

					UserDataHandler u = new UserDataHandler();
					u.editUser(pojo);
					try {
						res.sendRedirect("user");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					try {
						req.setAttribute("login_error", "Please login first");
						res.sendRedirect("logout");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else {
				try {
					req.setAttribute("login_error", "Please login first");
					res.sendRedirect("logout");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/uploadBulkUser", method = RequestMethod.POST)
	public void uploadFile(@RequestParam("file") MultipartFile file,HttpServletRequest request, HttpServletResponse response) {
		String message = null;
		try {
			if (ServletFileUpload.isMultipartContent(request)) {
				try {
					@SuppressWarnings("unchecked")
					File dir = new File("./" + File.separator + "tmpFiles");
					if (!dir.exists())
						dir.mkdirs();
					String name = new File(file.getOriginalFilename()).getName();
					
					File serverFile = new File(dir.getAbsolutePath()
							+ File.separator + name);
					BufferedOutputStream stream = new BufferedOutputStream(
							new FileOutputStream(serverFile));
					stream.write(file.getBytes());
					
					SimpleExcelReader simpleExcelReader = new SimpleExcelReader();
					message = simpleExcelReader.read(serverFile.getAbsolutePath());
					
					request.setAttribute("message", message);
				} catch (Exception ex) {
					request.setAttribute("message", "File Upload Failed due to " + ex);
				}

			} else {
				request.setAttribute("message", "File type must be xlsx.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if(message!=null && message.equalsIgnoreCase("Success")){
//				response.sendRedirect("user");
				if (request.getSession().getAttribute("user_id") != null) {
					UserDataHandler userDataHandler = new UserDataHandler();
					List<UserDetails> list = userDataHandler.getUserList(null, false);

					request.setAttribute("user", list);
					response.sendRedirect("user");
//					return "user-master";
				}
//				return "login";
			}else{
				
//				response.sendRedirect("");
//				UserController userController = new UserController();
//				userController.bulkUser(request, response);
				RequestDispatcher requestDispatcher; 
				requestDispatcher = request.getRequestDispatcher("bulkUser");
				requestDispatcher.forward(request, response);
//				return "bulk-user";
			}
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		return "bulk-user";
	}
	

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver commonsMultipartResolver() {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setDefaultEncoding("utf-8");
		commonsMultipartResolver.setMaxUploadSize(50000000);
		return commonsMultipartResolver;
	}
}
