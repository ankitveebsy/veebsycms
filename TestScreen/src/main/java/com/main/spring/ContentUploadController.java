package com.main.spring;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.main.db.CategoryHandler;
import com.main.db.MongoDBConnection;
import com.main.model.Category2Pojo;
import com.main.model.CategoryPojo;
import com.main.model.Product;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

@Controller
public class ContentUploadController {

	@RequestMapping(value = "/uploadContent", method = RequestMethod.GET)
	public String categoryMaster(HttpServletRequest req, HttpServletResponse res) {

		if (req.getSession().getAttribute("user_id") != null) {
			CategoryHandler categoryHandler = new CategoryHandler();
			List<CategoryPojo> list = categoryHandler.getData(null, false);

			if(req.getParameter("message")!=null && req.getParameter("message").equalsIgnoreCase("true"))
			req.setAttribute("message", "Data uploaded successfully.");
			req.setAttribute("categoryList", list);
			return "content-uploder";
		}
		return "login";
	}

	@RequestMapping(value = "/uploadContentData", method = RequestMethod.POST)
	public void uploadFile(Product product, BindingResult bindingResult,
			@RequestParam(value = "image", required = false) MultipartFile image, HttpServletRequest req,
			HttpServletResponse res) {
		String message = null;
		System.out.println("Name: " + product.getFileName());

		try {
			byte[] bytes = image.getBytes();

			Properties prop = new Properties();
			InputStream input = null;
			 input = new
			 FileInputStream("/usr/local/apache8/bin/demo.properties");
//			input = new FileInputStream("D:/VTS_Workspace/TestScreen/demo.properties");
			prop.load(input);
			String path = prop.getProperty("FILE_PATH").toString();
			
			String URL_PATH = prop.getProperty("URL_PATH").toString();
			
			String url = URL_PATH;
			if (product.getFileType().equalsIgnoreCase("Audio")) {
				path = path + "Audio/" + product.getCategoryName().toUpperCase().trim();
				url = url+"Audio/" + product.getCategoryName().toUpperCase().trim();
			} else if (product.getFileType().equalsIgnoreCase("Video")) {
				path = path + "Video/" + product.getCategoryName().toUpperCase().trim();
				url = url+"Video/" + product.getCategoryName().toUpperCase().trim();
			} else if (product.getFileType().equalsIgnoreCase("Image")) {
				path = path + "Image/" + product.getCategoryName().toUpperCase().trim();
				url = url+"Image/" + product.getCategoryName().toUpperCase().trim();
			}
			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			String filePath = dir.getAbsolutePath() + File.separator + product.getFileName() + "."
					+ image.getContentType().substring(image.getContentType().indexOf("/") + 1);
			
			
			url = url+"/"+product.getFileName() + "."
					+ image.getContentType().substring(image.getContentType().indexOf("/") + 1);
			
			// Create the file on server
			File serverFile = new File(filePath);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			insertData(product.getFileType(), product.getCategoryName().toUpperCase(), filePath, url,product.getDetails(),product.getFileName());
			res.sendRedirect("uploadContent?message=true");

		} catch (Exception e) {
			System.out.print(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void insertData(String dataType, String category, String path, String url,String dt,String name){
		try{
			DBCollection dbCollection = MongoDBConnection.getDB().getCollection("content_master");
			
			BasicDBObject basicDBObject = new BasicDBObject();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			CategoryHandler categoryHandler = new CategoryHandler();
			Category2Pojo p = categoryHandler.getDataByCat(category);
			BasicDBObject query = new BasicDBObject();
			query.put("category", p.getCategoryName());
			query.put("content_name", name);
			query.put("date", sdf.format(new Date()));
			
			DBCursor dbCursor = dbCollection.find(query);
			if(dbCursor.size()>0) {
				basicDBObject.put("exe_id", ""+System.currentTimeMillis());
				basicDBObject.put("category", p.getCategoryName());
				basicDBObject.put("category_image", p.getCategoryImage());
				basicDBObject.put("content_details", dt);
				basicDBObject.put("content_name", name);
				basicDBObject.put("data_type", dataType);
				basicDBObject.put("file_path", path);
				basicDBObject.put("file_url", url);
				basicDBObject.put("date", sdf.format(new Date()));
				basicDBObject.put("uploaded_time", new Date());
				dbCollection.update(query, new BasicDBObject("$set",basicDBObject), true, false);
				
			}else{
				basicDBObject.put("exe_id", ""+System.currentTimeMillis());
				basicDBObject.put("category", p.getCategoryName());
				basicDBObject.put("category_image", p.getCategoryImage());
				basicDBObject.put("content_details", dt);
				basicDBObject.put("content_name", name);
				basicDBObject.put("data_type", dataType);
				basicDBObject.put("file_path", path);
				basicDBObject.put("file_url", url);
				basicDBObject.put("date", sdf.format(new Date()));
				basicDBObject.put("uploaded_time", new Date());
				
				dbCollection.insert(basicDBObject);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver commonsMultipartResolver() {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setDefaultEncoding("utf-8");
		commonsMultipartResolver.setMaxUploadSize(50000000);
		return commonsMultipartResolver;
	}
}