package com.main.spring;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.main.db.CategoryHandler;
import com.main.db.MongoDBConnection;
import com.main.model.Category;
import com.main.model.CategoryPojo;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

@Controller
public class CategoryController {


	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String categoryMaster(HttpServletRequest req, HttpServletResponse res) {
		
		if (req.getSession().getAttribute("user_id") != null) {
			CategoryHandler categoryHandler = new CategoryHandler();
			List<CategoryPojo> list = categoryHandler.getData(null, false);
			if(req.getParameter("message")!=null && req.getParameter("message").equalsIgnoreCase("true"))
				req.setAttribute("message", "Data uploaded successfully.");
			
			if(req.getParameter("message")!=null && req.getParameter("message").equalsIgnoreCase("false"))
				req.setAttribute("message", "Failed to upload data.");
			
			
			req.setAttribute("categoryList", list);
			return "category-master";
		}
		return "login";
	}
	
	@RequestMapping(value = "/newCategory", method = RequestMethod.POST)
	public String newCategory(HttpServletRequest req, HttpServletResponse res) {
		
		if (req.getSession().getAttribute("user_id") != null) {
			return "new-category";
		}
		return "login";
	}
	
	/*@RequestMapping(value = "/addCategory", method = RequestMethod.POST)
	public String addCategory(HttpServletRequest req, HttpServletResponse res) {
		
		if (req.getSession().getAttribute("user_id") != null) {
			
			CategoryHandler categoryHandler = new CategoryHandler();
			categoryHandler.insertCategory(req.getParameter("categoryName"));
			
			List<CategoryPojo> list = categoryHandler.getData(null, false);

			req.setAttribute("categoryList", list);
			return "category-master";
		}
		return "login";
	}*/
	
	
	
	

	@RequestMapping(value = "/addCategory", method = RequestMethod.POST)
	public void uploadFile(Category categoryName, BindingResult bindingResult,
			@RequestParam(value = "image", required = false) MultipartFile image, HttpServletRequest req,
			HttpServletResponse res) {
		String message = null;

		try {
			byte[] bytes = image.getBytes();

			Properties prop = new Properties();
			InputStream input = null;
			 input = new
			 FileInputStream("/usr/local/apache8/bin/demo.properties");
//			input = new FileInputStream("D:/VTS_Workspace/TestScreen/demo.properties");
			prop.load(input);
			String path = prop.getProperty("FILE_PATH").toString();
			
			String URL_PATH = prop.getProperty("URL_PATH").toString();
			
			String url = URL_PATH;
			path = path + "CategoryImage/";
			url = url+"CategoryImage";
			
			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			String filePath = dir.getAbsolutePath() + File.separator + categoryName.getCategoryName() + "."
					+ image.getContentType().substring(image.getContentType().indexOf("/") + 1);
			
			
			url = url+"/"+categoryName.getCategoryName() + "."
					+ image.getContentType().substring(image.getContentType().indexOf("/") + 1);
			
			// Create the file on server
			File serverFile = new File(filePath);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			System.out.println("URL: "+url);
			
			
			CategoryHandler categoryHandler = new CategoryHandler();
			categoryHandler.insertCategory(req.getParameter("categoryName"),req.getParameter("categoryDetails"),url);
			res.sendRedirect("category?message=true");

		} catch (Exception e) {
			System.out.print(e.getMessage());
			e.printStackTrace();
			try {
				res.sendRedirect("category?message=false");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void insertData(String dataType, String category, String path, String url){
		try{
			DBCollection dbCollection = MongoDBConnection.getDB().getCollection("category_master");
			
			BasicDBObject basicDBObject = new BasicDBObject();
			basicDBObject.put("category", category);
			basicDBObject.put("data_type", dataType);
			basicDBObject.put("file_path", path);
			basicDBObject.put("file_url", url);
			
			dbCollection.insert(basicDBObject);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	

}
