package com.main.db;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.MongoClientOptions.Builder;
import com.mongodb.ServerAddress;

public class MongoDBConnection {

	public static MongoClient mongoDB;
	public static String db_name;
	public static String db_user;
	public static String db_password;
	public static DB db = null;
	public static String DB_IP;
	public static int DB_PORT;
	public static boolean IS_DB_CREDENTIAL_ENABLE;

	public static boolean makeDBConnection(String ip, int port) {
		boolean isOk = false;

		try {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream("/usr/local/apache8/bin/demo.properties");
//			input = new FileInputStream("D:/VTS_Workspace/TestScreen/demo.properties");
			prop.load(input);
			DB_IP = prop.getProperty("DB_IP").toString();
			DB_PORT = Integer.parseInt(prop.getProperty("DB_PORT"));
			db_name = prop.getProperty("DB_NAME");
			db_user = prop.getProperty("DB_USER");
			db_password = prop.getProperty("DB_PASSWORD");

			if (IS_DB_CREDENTIAL_ENABLE) {
				MongoCredential credential = MongoCredential.createCredential(db_user, db_name,
						db_password.toCharArray());
				mongoDB = new MongoClient(new ServerAddress(ip, port), Arrays.asList(credential));
			} else {
				mongoDB = new MongoClient(ip, MongoClientOptions.builder().socketTimeout(100000).connectTimeout(100000)
						.connectionsPerHost(5000).maxConnectionIdleTime(300000).maxWaitTime(300000).build());
			}

			// Builder o = MongoClientOptions.builder().connectTimeout(3000);
			// mongoDB = new MongoClient(new ServerAddress(ip, port),
			// o.connectionsPerHost(2000).maxConnectionIdleTime(300000).maxWaitTime(300000).build());

			db = mongoDB.getDB(db_name);
			System.out.println("db set");
			isOk = !mongoDB.getConnectPoint().isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isOk;
	}

	public static boolean isConnectionAlive() {
		DBObject ping = new BasicDBObject("ping", "1");
		try {
			MongoDBConnection.mongoDB.getDB("dbname").command(ping);
			return true;
		} catch (MongoException e) {
			return false;
		}
	}

	public static MongoClient getMongoClient() {

		return mongoDB;

	}

	public static DB getDB() {
		
		System.out.println("DB Object: "+db);
		if(db==null){
			if(makeDBConnection(DB_IP, DB_PORT)){
				System.out.println("DB Object 2: "+db);
				return db;
			}
		}else{
			return db;
		}
		System.out.println("DB Object 3: "+db);
		return null;
	}

	public static boolean stopMongoClient() {
		if (mongoDB != null)
			mongoDB.close();
		return true;
	}
}