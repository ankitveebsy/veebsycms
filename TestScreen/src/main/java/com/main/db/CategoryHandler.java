package com.main.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.main.model.Category2Pojo;
import com.main.model.CategoryPojo;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class CategoryHandler {

	
	public List<CategoryPojo> getData(String packageName, boolean isSearch){
		try{
			List<CategoryPojo> list = new ArrayList<CategoryPojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("category_master");
				BasicDBObject query = new BasicDBObject();
				if (isSearch) {
					query.put("category_name", Pattern.compile(packageName , Pattern.CASE_INSENSITIVE));
				}

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					CategoryPojo p = new CategoryPojo();
					p.setCategoryName(b.getString("category_name"));
					
					
					list.add(p);
				}
				return list;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Category2Pojo getDataByCat(String packageName){
		try{
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("category_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("category_name", Pattern.compile(packageName , Pattern.CASE_INSENSITIVE));
				

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					Category2Pojo p = new Category2Pojo();
					p.setCategoryName(b.getString("category_name"));
					p.setCategoryImage(b.getString("category_image"));
					
					return p;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public boolean insertCategory(String p, String des, String img){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("category_master");
				
				BasicDBObject query = new BasicDBObject();
				query.put("category_name",Pattern.compile(p , Pattern.CASE_INSENSITIVE));
				DBCursor dbCursor = table.find(query);
				if(dbCursor.size()==0){
					
					BasicDBObject d = new BasicDBObject();
					d.put("category_name",p.trim());
					d.put("category_image",img);
					d.put("category_description",des);
					d.put("created_time",new Date());
					d.put("updated_time",new Date());
					table.insert(d);
					
				}
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
}
