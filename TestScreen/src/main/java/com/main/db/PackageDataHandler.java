package com.main.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.main.model.PackagePojo;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class PackageDataHandler {
	
	public List<PackagePojo> getPackageData(String packageName, boolean isSearch){
		try{
			List<PackagePojo> list = new ArrayList<PackagePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("package_master");
				BasicDBObject query = new BasicDBObject();
				if (isSearch) {
					query.put("package_name", Pattern.compile(packageName , Pattern.CASE_INSENSITIVE));
				}

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					PackagePojo p = new PackagePojo();
					p.setPackageCost("$ "+b.getString("package_cost"));
					p.setPackageDuration(b.getString("package_duration")+" Days");
					p.setPackageName(b.getString("package_name"));
					list.add(p);
				}
				return list;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addPackage(PackagePojo p){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("package_master");
				BasicDBObject d = new BasicDBObject();
				
				BasicDBObject query = new BasicDBObject();
				query.put("package_name", Pattern.compile(p.getPackageName() , Pattern.CASE_INSENSITIVE));
				DBCursor dbCursor = table.find(query);
				if(dbCursor.size()==0){
					d.put("package_cost", p.getPackageCost());
					d.put("package_duration", p.getPackageDuration());
					d.put("package_name", p.getPackageName());
					d.put("created_time",new Date());
					d.put("updated_time",new Date());
					table.insert(d);
					return true;
				}
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void editPackage(PackagePojo p){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("package_master");
				
				BasicDBObject query = new BasicDBObject();
				query.put("package_name", Pattern.compile(p.getPackageName() , Pattern.CASE_INSENSITIVE));
				
				BasicDBObject d = new BasicDBObject();
				d.put("package_cost", p.getPackageCost());
				d.put("package_duration", p.getPackageDuration());
				d.put("package_name", p.getPackageName());
				d.put("updated_time",new Date());
				table.update(query, new BasicDBObject("$set",d));
				
				CorporateHandler corporateHandler = new CorporateHandler();
				corporateHandler.updateCorporateDueToPackage(p);
				
				UserDataHandler userDataHandler = new UserDataHandler();
				userDataHandler.editUserDueToPackage(p);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public PackagePojo getPackage(String packageName){
		try{
			List<PackagePojo> list = new ArrayList<PackagePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("package_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("package_name", packageName);
				

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					PackagePojo p = new PackagePojo();
					p.setPackageCost(b.getString("package_cost"));
					p.setPackageDuration(b.getString("package_duration"));
					p.setPackageName(b.getString("package_name"));
					return p;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean removePackage(String packageName){
		try{
			List<PackagePojo> list = new ArrayList<PackagePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("subscription_name", packageName);
				
				DBCursor dbCursor = table.find(query);
				if(dbCursor!=null && dbCursor.size()>0){
					return false;
				}
				
				table = db.getCollection("corporate_master");
				
				query = new BasicDBObject();
				query.put("package_name",packageName);
				
				dbCursor = table.find(query);
				if(dbCursor!=null && dbCursor.size()>0){
					return false;
				}
				
				table = db.getCollection("package_master");
			    query = new BasicDBObject();
				
				query.put("package_name", packageName);
				table.remove(query);
				
				return true;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	

}
