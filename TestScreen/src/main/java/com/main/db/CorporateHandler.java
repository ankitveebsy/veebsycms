package com.main.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.main.model.CorporatePojo;
import com.main.model.PackagePojo;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class CorporateHandler {

	
	public List<CorporatePojo> getCorporateData(String packageName, boolean isSearch){
		try{
			List<CorporatePojo> list = new ArrayList<CorporatePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				BasicDBObject query = new BasicDBObject();
				if (isSearch) {
					query.put("corporate_name", Pattern.compile(packageName , Pattern.CASE_INSENSITIVE));
				}

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					CorporatePojo p = new CorporatePojo();
					p.setCompanyName(b.getString("corporate_name"));
					p.setCountryName("N/A");
					p.setPackageName(b.getString("package_name"));
					p.setSubscriptionEnd(b.getString("subscription_end"));
					p.setSubscriptionStart(b.getString("subscription_start"));
					p.setTotalUser(b.getString("total_user"));
					
					
					list.add(p);
				}
				return list;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public CorporatePojo getCorporate(String packageName){
		try{
			List<CorporatePojo> list = new ArrayList<CorporatePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				BasicDBObject query = new BasicDBObject();
				query.put("corporate_name", packageName);

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					CorporatePojo p = new CorporatePojo();
					p.setCompanyName(b.getString("corporate_name"));
					p.setCountryName("N/A");
					p.setPackageName(b.getString("package_name"));
					p.setSubscriptionEnd(b.getString("subscription_end"));
					p.setSubscriptionStart(b.getString("subscription_start"));
					p.setTotalUser(b.getString("total_user"));
					
					return p;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public CorporatePojo getCorporateValidation(String packageName){
		try{
			List<CorporatePojo> list = new ArrayList<CorporatePojo>();
			
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				BasicDBObject query = new BasicDBObject();
				query.put("corporate_name", Pattern.compile(packageName , Pattern.CASE_INSENSITIVE));

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					CorporatePojo p = new CorporatePojo();
					p.setCompanyName(b.getString("corporate_name"));
					p.setCountryName("N/A");
					p.setPackageName(b.getString("package_name"));
					p.setSubscriptionEnd(b.getString("subscription_end"));
					p.setSubscriptionStart(b.getString("subscription_start"));
					p.setTotalUser(b.getString("total_user"));
					
					return p;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addCorporate(CorporatePojo p){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				BasicDBObject d = new BasicDBObject();
				
				
				BasicDBObject query = new BasicDBObject();
				query.put("corporate_name", p.getCompanyName());
				DBCursor dbCursor = table.find(query);
				if(dbCursor.size()==0){
					
					d.put("corporate_name", p.getCompanyName());
					d.put("package_name", p.getPackageName());
					d.put("total_user", p.getTotalUser());
					d.put("subscription_start", p.getSubscriptionStart());
					
					System.out.println("Date: "+p.getSubscriptionStart()+"----"+p.getPackageName());
					PackageDataHandler pp = new PackageDataHandler();
					PackagePojo packagePojo = pp.getPackage(p.getPackageName());
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sdf.parse(p.getSubscriptionStart()));
					calendar.add(Calendar.DATE, Integer.parseInt(packagePojo.getPackageDuration()));
					d.put("subscription_end", sdf.format(calendar.getTime()));
					d.put("created_time",new Date());
					d.put("updated_time",new Date());
					table.insert(d);
					return true;
				}
				
				
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}


	
	public boolean editCorporate(CorporatePojo p){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				DBCollection packageTable = db.getCollection("package_master");
				BasicDBObject d = new BasicDBObject();
				
				BasicDBObject queryUpdate = new BasicDBObject();
				queryUpdate.put("corporate_name",p.getCompanyName());
				
					d.put("corporate_name", p.getCompanyName());
					d.put("package_name", p.getPackageName());
					d.put("total_user", p.getTotalUser());
					d.put("subscription_start", p.getSubscriptionStart());
					
					PackageDataHandler pp = new PackageDataHandler();
					PackagePojo packagePojo = pp.getPackage(p.getPackageName());
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sdf.parse(p.getSubscriptionStart()));
					calendar.add(Calendar.DATE, Integer.parseInt(packagePojo.getPackageDuration()));
					d.put("subscription_end", sdf.format(calendar.getTime()));
					d.put("updated_time",new Date());
					table.update(queryUpdate, new BasicDBObject("$set",d));
					
					updateUserData(p.getCompanyName(), p.getSubscriptionStart(), sdf.format(calendar.getTime()), p.getPackageName());
					return true;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	public void updateUserData(String cName,String start,String end,String pac){
		DB db = MongoDBConnection.getDB();
		if (db != null) {
			DBCollection table = db.getCollection("user_master");
			
			BasicDBObject queryUpdate = new BasicDBObject();
			queryUpdate.put("corporate_name",cName);
			
			BasicDBObject updateQuery = new BasicDBObject();
			updateQuery.put("subscription_start", start);
			updateQuery.put("subscription_end", end);
			updateQuery.put("subscription_name", pac);
			
			table.update(queryUpdate, new BasicDBObject("$set",updateQuery), false, true);
		}
	}
	
	
	public void updateCorporateDueToPackage(PackagePojo pojo){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("corporate_master");
				
				BasicDBObject query = new BasicDBObject();
				query.put("package_name",pojo.getPackageName());
				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					BasicDBObject updateQuery = new BasicDBObject();
					updateQuery.put("corporate_name",b.getString("corporate_name"));
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					
					BasicDBObject up = new BasicDBObject();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sdf.parse(b.getString("subscription_start")));
					calendar.add(Calendar.DATE, Integer.parseInt(pojo.getPackageDuration()));
					up.put("subscription_end", sdf.format(calendar.getTime()));
					
					
					table.update(updateQuery, new BasicDBObject("$set",up));
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
