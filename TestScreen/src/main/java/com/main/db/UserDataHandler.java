package com.main.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.main.model.PackagePojo;
import com.main.model.UserDetails;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class UserDataHandler {

	public List<UserDetails> getUserList(String data,  boolean isSearch) {
		try {
			List<UserDetails> userList = new ArrayList<UserDetails>();
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				query.put("is_active", true);
				
				if (isSearch) {
					query.put("name", java.util.regex.Pattern.compile(data));
				}

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject) dbCursor.next();
					UserDetails user = new UserDetails();
					user.setName(b.getString("name") == null ? "N/A" : b.getString("name"));
					user.setCorporateName(
							b.getString("corporate_name") == null ? "N/A" : b.getString("corporate_name"));
					user.setEmailID(b.getString("email_id") == null ? "N/A" : b.getString("email_id"));
					user.setMob(b.getString("mobile") == null ? "N/A" : b.getString("mobile"));
					user.setSubscriptionStartDate(
							b.getString("subscription_start") == null ? "N/A" : b.getString("subscription_start"));
					user.setSubscriptionEndDate(
							b.getString("subscription_end") == null ? "N/A" : b.getString("subscription_end"));
					user.setId("" + b.getInt("user_id") == null ? "N/A" : "" + b.getInt("user_id"));
					user.setSubscriptionPackageName(
							b.getString("subscription_name") == null ? "N/A" : b.getString("subscription_name"));
					user.setUserType(b.getString("user_type") == null ? "N/A" : b.getString("user_type"));
					user.setSubscriptionPackageCost(
							b.getString("subscription_cost") == null ? "N/A" : b.getString("subscription_cost"));
					
					userList.add(user);
				}
				return userList;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public UserDetails getUserData(String data) {
		try {
			System.out.println("Get User Data: "+data);
			List<UserDetails> userList = new ArrayList<UserDetails>();
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				query.put("email_id", java.util.regex.Pattern.compile(data));
					
				DBCursor dbCursor = table.find(query);
				System.out.println("Get User Data: "+data+" after query: "+dbCursor.size());
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject) dbCursor.next();
					UserDetails user = new UserDetails();
					user.setName(b.getString("name") == null ? "N/A" : b.getString("name"));
					user.setCorporateName(
							b.getString("corporate_name") == null ? "N/A" : b.getString("corporate_name"));
					user.setEmailID(b.getString("email_id") == null ? "N/A" : b.getString("email_id"));
					user.setMob(b.getString("mobile") == null ? "N/A" : b.getString("mobile"));
					user.setSubscriptionStartDate(
							b.getString("subscription_start") == null ? "N/A" : b.getString("subscription_start"));
					user.setSubscriptionEndDate(
							b.getString("subscription_end") == null ? "N/A" : b.getString("subscription_end"));
					user.setId("" + b.getInt("user_id") == null ? "N/A" : "" + b.getInt("user_id"));
					user.setSubscriptionPackageName(
							b.getString("subscription_name") == null ? "N/A" : b.getString("subscription_name"));
					user.setUserType(b.getString("user_type") == null ? "N/A" : b.getString("user_type"));
					user.setSubscriptionPackageCost(
							b.getString("subscription_cost") == null ? "N/A" : b.getString("subscription_cost"));
					
					return user;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public void mapUserWithSubscription(String email, String subscription) {
		try {
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("subscription_master");
				BasicDBObject query = new BasicDBObject();
				query.put("subscription_name", subscription);

				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject) dbCursor.next();
					int days = b.getInt("subscription_duration");
					String subscription_name = b.getString("subscription_name");
					String subscription_cost = b.getString("subscription_cost");

					BasicDBObject updateQuery = new BasicDBObject();
					updateQuery.put("email_id", email);

					BasicDBObject update = new BasicDBObject();
					update.put("subscription_name", subscription_name);
					update.put("subscription_cost", subscription_cost);
					update.put("subscription_start", sdf.format(new Date()));
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					cal.add(Calendar.DATE, days);

					update.put("subscription_end", sdf.format(cal.getTime()));

					DBCollection table2 = db.getCollection("user_master");
					table2.update(updateQuery, new BasicDBObject("$set", update));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean removeUser(String user) {
		try {
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				query.put("email_id", user);
				
//				BasicDBObject update = new BasicDBObject();
//				update.put("is_active", false);
				table.remove(query);
//				table.update(query, new BasicDBObject("$set",update));
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void addNewUser(UserDetails user){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("user_master");
				
				BasicDBObject query = new BasicDBObject();
				query.put("email_id", user.getEmailID());
				
				BasicDBObject d = new BasicDBObject();
				d.put("name", user.getName());
				d.put("user_id", getUserID());
				d.put("email_id", user.getEmailID());
				d.put("corporate_name", user.getCorporateName());
				d.put("mobile", user.getMob());
				d.put("subscription_start", user.getSubscriptionStartDate());
				d.put("subscription_end", user.getSubscriptionEndDate());
				d.put("subscription_name", user.getSubscriptionPackageName());
				d.put("subscription_cost", user.getSubscriptionPackageCost());
				d.put("user_type", user.getUserType());
				d.put("password", "newpass123");
				d.put("is_active", true);
				d.put("created_by", "CMS");
				d.put("created_time", new Date());
				d.put("updated_time", new Date());
				table.update(query, new BasicDBObject("$set",d),true,false);
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void editUser(UserDetails user){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("email_id", user.getEmailID());
				
				BasicDBObject d = new BasicDBObject();
				d.put("name", user.getName());
				d.put("email_id", user.getEmailID());
				
				d.put("corporate_name", user.getCorporateName());
				d.put("mobile", user.getMob());
				d.put("subscription_start", user.getSubscriptionStartDate());
				d.put("subscription_end", user.getSubscriptionEndDate());
				d.put("subscription_name", user.getSubscriptionPackageName());
				System.out.println("Sub: "+ user.getSubscriptionPackageName());
				d.put("subscription_cost", user.getSubscriptionPackageCost());
				d.put("user_type", user.getUserType());
				d.put("is_active", true);
				d.put("updated_time", new Date());
				table.update(query,new BasicDBObject("$set",d));
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public int getUserID(){
		int id = 1;
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection dbCollection = db.getCollection("user_master");
				
				DBCursor dbCursor = dbCollection.find().sort(new BasicDBObject("user_id",-1)).limit(1);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					return b.getInt("user_id")+1;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return id;
	}
	
	public boolean isEmailAlreadyUsed(String email){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection dbCollection = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("email_id", Pattern.compile(email.trim() , Pattern.CASE_INSENSITIVE));
				
				System.out.println("Query : "+query.toJson());
				DBCursor dbCursor = dbCollection.find(query);
				if(dbCursor.size()>0){
					return true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public void editUserDueToPackage(PackagePojo pojo){
		try{
			DB db = MongoDBConnection.getDB();
			if (db != null) {
				DBCollection table = db.getCollection("user_master");
				BasicDBObject query = new BasicDBObject();
				
				query.put("subscription_name", pojo.getPackageName());
				DBCursor dbCursor = table.find(query);
				while (dbCursor.hasNext()) {
					BasicDBObject b = (BasicDBObject)dbCursor.next();
					
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					
					BasicDBObject up = new BasicDBObject();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sdf.parse(b.getString("subscription_start")));
					calendar.add(Calendar.DATE, Integer.parseInt(pojo.getPackageDuration()));
					up.put("subscription_end", sdf.format(calendar.getTime()));
					table.update(query,new BasicDBObject("$set",up));
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}