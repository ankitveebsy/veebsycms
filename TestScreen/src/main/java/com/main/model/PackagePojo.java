package com.main.model;

public class PackagePojo {
	
	String packageName;
	String packageCost;
	String packageDuration;
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(String packageCost) {
		this.packageCost = packageCost;
	}
	public String getPackageDuration() {
		return packageDuration;
	}
	public void setPackageDuration(String packageDuration) {
		this.packageDuration = packageDuration;
	}
}