package com.main.model;

public class CorporatePojo {
	
	String companyName;
	String countryName;
	String subscriptionStart;
	String subscriptionEnd;
	String totalUser;
	String packageName;
	
	
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getSubscriptionStart() {
		return subscriptionStart;
	}
	public void setSubscriptionStart(String subscriptionStart) {
		this.subscriptionStart = subscriptionStart;
	}
	public String getSubscriptionEnd() {
		return subscriptionEnd;
	}
	public void setSubscriptionEnd(String subscriptionEnd) {
		this.subscriptionEnd = subscriptionEnd;
	}
	public String getTotalUser() {
		return totalUser;
	}
	public void setTotalUser(String totalUser) {
		this.totalUser = totalUser;
	}
	
	
}
