package com.main.model;

import org.springframework.web.multipart.MultipartFile;

public class Product
{
    String fileName;
    String categoryName;
    String fileType;
    String details;
    int attempt;
//    MultipartFile file;

    
	public String getFileName() {
		return fileName;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

//	public MultipartFile getFile() {
//		return file;
//	}
//
//	public void setFile(MultipartFile file) {
//		this.file = file;
//	}
	
}