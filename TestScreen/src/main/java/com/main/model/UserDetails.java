package com.main.model;

public class UserDetails {

	String name="";
	String id="";
	String mob="";
	String emailID="";
	String userType="";
	String subscriptionStartDate="";
	String subscriptionEndDate="";
	String subscriptionPackageName="";
	String subscriptionPackageCost="";
	String password="";
	String corporateName="";
	
	
	
	public String getSubscriptionPackageName() {
		return subscriptionPackageName;
	}
	public void setSubscriptionPackageName(String subscriptionPackageName) {
		this.subscriptionPackageName = subscriptionPackageName;
	}
	public String getSubscriptionPackageCost() {
		return subscriptionPackageCost;
	}
	public void setSubscriptionPackageCost(String subscriptionPackageCost) {
		this.subscriptionPackageCost = subscriptionPackageCost;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getSubscriptionStartDate() {
		return subscriptionStartDate;
	}
	public void setSubscriptionStartDate(String subscriptionStartDate) {
		this.subscriptionStartDate = subscriptionStartDate;
	}
	public String getSubscriptionEndDate() {
		return subscriptionEndDate;
	}
	public void setSubscriptionEndDate(String subscriptionEndDate) {
		this.subscriptionEndDate = subscriptionEndDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMob() {
		return mob;
	}
	public void setMob(String mob) {
		this.mob = mob;
	}
	
	
}
