<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.main.spring.UserController"%>
<%@page import="com.main.model.PackagePojo"%>
<%@page import="com.main.model.CorporatePojo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> -->
<script type="text/javascript">
	function show() {
		document.getElementById('selection').style.display = 'block';
		document.getElementById('selection').style.display = 'none';

		document.getElementById('package').style.display = 'none';
		document.getElementById('package').style.display = 'block';

		document.getElementById('start').style.display = 'none';
		document.getElementById('start').style.display = 'block';
	}
	function show2() {

		document.getElementById('selection').style.display = 'none';
		document.getElementById('selection').style.display = 'block';

		document.getElementById('package').style.display = 'block';
		document.getElementById('package').style.display = 'none';

		document.getElementById('start').style.display = 'block';
		document.getElementById('start').style.display = 'none';

	}
</script>

<%@ page language="java"
	import="java.util.*,java.lang.*,com.main.model.UserDetails,com.mongodb.BasicDBObject,com.main.db.MongoDBConnection,com.mongodb.DB,com.mongodb.DBCollection,com.mongodb.DBCursor"%>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Veebsy - Add User</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/icon/favicon.ico">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap-datepicker.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/themify-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/metisMenu.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/owl.carousel.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/slicknav.min.css">
<!-- others css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/typography.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/default-css.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/responsive.css">
<!-- modernizr css -->
<script
	src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<!-- preloader area start -->
	<div id="preloader">
		<div class="loader"></div>
	</div>
	<!-- preloader area end -->
	<!-- page container area start -->
	<div class="page-container">
		<!-- sidebar menu area start -->
		<div class="sidebar-menu">
			<div class="sidebar-header">
				<div class="logo">
					<a href="${pageContext.request.contextPath}/dashboard"><img
						src="${pageContext.request.contextPath}/resources/images/icon/logo.png"
						alt="logo"></a>
				</div>
			</div>
			<div class="main-menu">
				<div class="menu-inner">
					<nav>
					<ul class="metismenu" id="menu">
						<li><a href="${pageContext.request.contextPath}/dashboard"><i
								class="ti-dashboard"></i><span>Dashboard</span></a></li>
						<li><a href="${pageContext.request.contextPath}/corporate"><i
								class="ti-user"></i> <span>Corporate Master</span></a></li>
						<li class="active"><a
							href="${pageContext.request.contextPath}/user"><i
								class="ti-user"></i> <span>User Master</span></a></li>
						<li><a href="${pageContext.request.contextPath}/package"><i
								class="ti-files"></i> <span>Package &amp; Plans Master</span></a></li>
						<li><a href="${pageContext.request.contextPath}/category"><i
								class="ti-files"></i> <span>Category Master</span></a></li>
						<li><a
							href="${pageContext.request.contextPath}/uploadContent"><i
								class="ti-files"></i> <span>Upload Content</span></a></li>
						<li><a href="${pageContext.request.contextPath}/logout"><i
								class="fa fa-sign-out"></i> <span>Logout</span></a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- sidebar menu area end -->
		<!-- main content area start -->
		<div class="main-content">
			<!-- header area start -->
			<div class="header-area">
				<div class="row align-items-center">
					<div class="navLogo">
						<a><img
							src="${pageContext.request.contextPath}/resources/images/icon/logo2.png"
							alt="" /></a>
					</div>

					<!-- nav and search button -->
					<div class="col-md-6 col-sm-8 clearfix">
						<div class="nav-btn pull-left">
							<span></span> <span></span> <span></span>
						</div>
					</div>
					<!-- profile info & task notification -->

				</div>
			</div>
			<!-- header area end -->
			<!-- page title area start -->
			<div class="page-title-area">
				<div class="row align-items-center">
					<div class="col-sm-6">
						<div class="breadcrumbs-area clearfix">
							<h4 class="page-title pull-left">Add New User</h4>
						</div>
					</div>
					<%-- <div class="col-sm-6 clearfix">
          <div class="user-profile pull-right"> <img class="avatar user-thumb" src="${pageContext.request.contextPath}/resources/images/author/avatar.png" alt="avatar">
            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Kumkum Rai</h4>
          </div>
        </div> --%>
				</div>
			</div>

			<%
				List<CorporatePojo> cList = (List<CorporatePojo>) request.getAttribute("companyList");
				List<PackagePojo> pList = (List<PackagePojo>) request.getAttribute("packageList");

				if (pList != null && pList.size() > 0) {
			%>


			<!-- page title area end -->
			<div class="main-content-inner">
				<div class="row">
					<div class="col-12">
						<div class="card mt-5">
							<div class="card-body">
								<form action="addUser" method="POST">
									<div class="form-row">
										<div class="col-md-4 mb-3">
											<label class="col-form-label">Name</label> <input type="text"
												class="form-control" name="Name" required="required"
												placeholder="Name">
										</div>
										<div class="col-md-4 mb-3">
											<label class="col-form-label">Email ID</label> <input
												type="text" class="form-control" placeholder="Email ID"
												id="email" name="Email" required="required">

											<%
												String error = (String) request.getAttribute("error");
													if (error != null) {
											%>
											<%=error%>
											<%
												}
											%>
										</div>
										<div class="col-md-4 mb-3">
											<label class="col-form-label">Mobile No.</label> <input
												type="text" class="form-control" name="Mobile">
										</div>
									</div>
									<div class="form-row">
										<div class="col-md-4 mb-3">
											<label class="col-form-label">User Type</label>
											<div class="custom-control custom-radio">
												<input type="radio" id="custom1"
													class="custom-control-input" name="userType"
													value="Individual" onchange="show()" checked> <label
													class="custom-control-label" for="custom1">Individual</label>
											</div>
											<div class="custom-control custom-radio">
												<input type="radio" id="custom2" name="userType"
													onchange="show2()" value="Corporate"
													class="custom-control-input"> <label
													class="custom-control-label" for="custom2">Corporate</label>
											</div>
										</div>

										<%
											if (cList != null && cList.size() > 0) {
										%>


										<div class="col-md-4 mb-3" id="selection"
											style="display: none">
											<label class="col-form-label">Select Corporate</label> <select
												class="form-control" name="company">
												<%
													for (int i = 0; i < cList.size(); i++) {
																String cName = cList.get(i).getCompanyName();
																if (i == 0) {
												%>

												<option value="<%=cName%>" selected="selected"><%=cName%></option>
												<%
													} else {
												%>
												<option value="<%=cName%>"><%=cName%></option>

												<%
													}

															}
												%>
											</select>
										</div>
										<%
											} else {
										%>
										<div class="col-md-4 mb-3">
											<p>No corporate to use.</p>
										</div>
										<%
											}
										%>










										<div class="col-md-4 mb-3" id="package">
											<label class="col-form-label">Select Package</label> <select
												class="form-control" name="package">
												<%
													System.out.println("P List: " + pList.size());
														for (int i = 0; i < pList.size(); i++) {
															String pName = pList.get(i).getPackageName();
															if (i == 0) {
												%>

												<option value="<%=pName%>" selected="selected"><%=pName%></option>
												<%
													} else {
												%>
												<option value="<%=pName%>"><%=pName%></option>

												<%
													}

														}
												%>
											</select>
										</div>

										<%
											Date date = new Date();
												SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
												String dt = sdf.format(date);
										%>
										<div class="col-md-4 mb-3" id="start">
											<label for="example-date-input" class="col-form-label">Date</label>
											<div class="input-group">
												<input class="form-control border-right-0 datepicker"
													type="text" value="<%=dt%>" name="startDate">
												<span class="input-group-addon border-left-0"><i
													class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
									<button class="btn btn-primary" type="submit">Submit</button>
								</form>

								<%
									} else {
								%>
								<div>
									<h3>No Package to use.Please create a Package first.</h3>
								</div>
								<%
									}
								%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- main content area end -->
		<!-- footer area start-->
		<footer>
		<div class="footer-area">
			<p>© Copyright 2018. All right reserved.</p>
		</div>
		</footer>
		<!-- footer area end-->
	</div>
	<!-- page container area end -->
	<!-- jquery latest version -->
	<script
		src="${pageContext.request.contextPath}/resources/js/vendor/jquery-2.2.4.min.js"></script>
	<!-- bootstrap 4 js -->
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/owl.carousel.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/metisMenu.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slicknav.min.js"></script>
	<!-- others plugins -->
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
	<script>
		$(function() {
			$(".datepicker").datepicker({
				autoclose : true,
				todayHighlight : true
			});
		});
	</script>
</body>
</html>
