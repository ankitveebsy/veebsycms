<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<%@ page language="java"
	import="java.util.*"%>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png"
	href="assets/images/icon/favicon.ico">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/themify-icons.css">
<!-- others css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/typography.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/default-css.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/responsive.css">
<!-- modernizr css -->
<script
	src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
<script type="text/javascript">
	function validateform() {
		var name = document.myform.emailID.value;
		var password = document.myform.password.value;

		if (name == null || name == "") {
			alert("Please enter Login ID");
			return false;
		} else if (password == null || password == "") {
			alert("Please enter Password");
			return false;
		} else {
			if (name == "admin" && password == "admin@123") {
				return true;
			} else {
				alert("Invalid user ID/Password");
				return false;
			}
		}
	}
</script>
</head>

<body>
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<!-- preloader area start -->
	<div id="preloader">
		<div class="loader"></div>
	</div>

	<!-- preloader area end -->
	<!-- login area start -->
	<div class="login-area login-bg">
		<div class="container">
			<div class="login-box ptb--100">
				<form action="dashboard" method="Post" name="myform"
					class="login100-form validate-form"
					onsubmit="return validateform()">
					<div class="login-form-head">
						<h4>
							<img
								src="${pageContext.request.contextPath}/resources/images/icon/logo2.png"
								alt="" />
						</h4>
					</div>
					<div class="login-form-body">
						<div class="form-gp">
							<label for="exampleInputEmail1">User ID</label></br> <input
								type="text" name="emailID"> <i class="ti-email"></i>
						</div>
						<div class="form-gp">
							<label for="exampleInputPassword1">Password</label></br> <input
								type="password" name="password"> <i class="ti-lock"></i>
						</div>
						<div class="row mb-4 rmber-area">
							<!--<div class="col-6">
              <div class="custom-control custom-checkbox mr-sm-2">
                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
              </div>
            </div>-->


							<!-- 
            <div class="col-12 text-right"> <a href="#">Forgot Password?</a> </div>
             -->

						</div>
						<div class="submit-btn-area">
							<button class="btn-primary" id="form_submit" type="submit">
								Submit <i class="ti-arrow-right"></i>
							</button>
						</div>
						<!--<div class="form-footer text-center mt-5">
            <p class="text-muted">Don't have an account? <a href="register.html">Sign up</a></p>
          </div>-->
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- login area end -->

	<!-- jquery latest version -->
	<script
		src="${pageContext.request.contextPath}/resources/js/vendor/jquery-2.2.4.min.js"></script>
	<!-- bootstrap 4 js -->
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>

	<!-- others plugins -->
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
</body>
</html>