<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<%@ page language="java"
	import="java.util.*,java.lang.*,com.main.model.PackagePojo"%>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Veebsy - Package</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/icon/favicon.ico">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/themify-icons.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/metisMenu.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/owl.carousel.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/slicknav.min.css">
<!-- others css -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/typography.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/default-css.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/responsive.css">
<!-- modernizr css -->
<script
	src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
<script type="text/javascript">
	
	function callServlet(message) {
		
		alert(message);
	}
</script>
</head>
<body>
<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<!-- preloader area start -->
<div id="preloader">
  <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
  <!-- sidebar menu area start -->
  <div class="sidebar-menu">
    <div class="sidebar-header">
      <div class="logo"> <a href="${pageContext.request.contextPath}/dashboard"><img
						src="${pageContext.request.contextPath}/resources/images/icon/logo.png"
						alt="logo"></a> </div>
    </div>
    <div class="main-menu">
      <div class="menu-inner">
        <nav>
          <ul class="metismenu" id="menu">
            <li><a href="${pageContext.request.contextPath}/dashboard"><i
								class="ti-dashboard"></i><span>Dashboard</span></a></li>
            <li ><a href="${pageContext.request.contextPath}/corporate"><i
								class="ti-user"></i> <span>Corporate Master</span></a></li>
            <li><a href="${pageContext.request.contextPath}/user"><i
								class="ti-user"></i> <span>User Master</span></a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/package"><i
								class="ti-files"></i> <span>Package &amp; Plans Master</span></a></li>
            <li><a href="${pageContext.request.contextPath}/category"><i
								class="ti-files"></i> <span>Category Master</span></a></li>
            <li><a
							href="${pageContext.request.contextPath}/uploadContent"><i
								class="ti-files"></i> <span>Upload Content</span></a></li>
            <li><a href="${pageContext.request.contextPath}/logout"><i
								class="fa fa-sign-out"></i> <span>Logout</span></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <!-- sidebar menu area end -->
  <!-- main content area start -->
  <div class="main-content">
    <!-- header area start -->
    <div class="header-area">
      <div class="row align-items-center">
        <!-- nav and search button -->
		<div class="navLogo"><a><img src="${pageContext.request.contextPath}/resources/images/icon/logo2.png" alt=""/></a></div>
        <div class="col-md-6 col-sm-8 clearfix">
          <div class="nav-btn pull-left"> <span></span> <span></span> <span></span> </div>
        </div>
        <!-- profile info & task notification -->
      </div>
    </div>
    <!-- header area end -->
    <!-- page title area start -->
    <div class="page-title-area">
      <div class="row align-items-center">
        <div class="col-sm-6">
          <div class="breadcrumbs-area clearfix">
            <h4 class="page-title pull-left">Package &amp; Plans Master</h4>
          </div>
        </div>
        <%-- <div class="col-sm-6 clearfix">
						<div class="user-profile pull-right">
							<img class="avatar user-thumb"
								src="${pageContext.request.contextPath}/resources/images/author/avatar.png"
								alt="avatar">
							<h4 class="user-name dropdown-toggle" data-toggle="dropdown">Kumkum
								Rai</h4>
						</div>
					</div> --%>
      </div>
    </div>
    <!-- page title area end -->
    <div class="main-content-inner">
      <div class="row justify-content-md-center">
        <div class="col col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 col-lg-6">
              <form action="searchPackage" method="POST">
                <div class="input-group">
                  <input type="text" class="form-control" name="packageToSearch"
											required="required" placeholder="Enter Package Name">
                  <div class="input-group-append">
                    <input type="submit" class="btn btn-primary" name="Submit"
												value="Search">
                  </div>
                </div>
              </form>
                </div>
                <div class="col-sm-12 col-lg-6 text-right mobMarTop">
          <form action="newPackage" method="post"><input type="submit" class="btn btn-primary" name="Submit" value="Add Package"></form></div>
              </div>
            </div>
          </div>
        </div>
        
        <%
						List<PackagePojo> list = (List<PackagePojo>) request.getAttribute("user");
					%>
        <!-- basic table start -->
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <div class="single-table">
                <div class="table-responsive">
                  <table class="table text-center">
                    <thead class="text-uppercase">
                      <tr>
                        <th scope="col">Sr No</th>
                        <th scope="col">Package Name</th>
                        <th scope="col">Package Duration</th>
                        <th scope="col">Package Cost</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <%
													int i = 0;
													if (list != null)
														for (PackagePojo pojo : list) {
															i++;
												%>
                      <tr>
                        <td style="text-align: center; width: 40px"><%
															out.print(i);
														%>
                        </td>
                        <td style="text-align: center; width: 40px"><%
															out.print(pojo.getPackageName());
														%>
                        </td>
                        <td style="text-align: center; width: 40px"><%
															out.print(pojo.getPackageDuration());
														%>
                        </td>
                        <td style="text-align: center; width: 40px"><%
															out.print(pojo.getPackageCost());
														%>
                          </br>
                        </td>
                        <td style="text-align: center; width: 40px"><%
															String packageName = pojo.getPackageName();
														%>
                          <a href="editPackage?packageName=<%=packageName%>"> <i class="ti-pencil"></i> </a>
                          <%--  <a href="removePackage?packageName=<%=packageName%>">
															<i class="ti-trash"></i>
													</a> --%>
                        </td>
                      </tr>
                      <%
													}
												%>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <%
			String message = (String) request.getAttribute("message");
			
			if (message != null) {
		%>
  <script>
			callServlet(<%=message%>);
		</script>
  <%
			}
		%>
  <!-- main content area end -->
  <!-- footer area start-->
  <footer>
    <div class="footer-area">
      <p>© Copyright 2018. All right reserved.</p>
    </div>
  </footer>
  <!-- footer area end-->
</div>
<!-- page container area end -->
<!-- jquery latest version -->
<script
		src="${pageContext.request.contextPath}/resources/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap 4 js -->
<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/owl.carousel.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/metisMenu.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slicknav.min.js"></script>
<!-- others plugins -->
<script
		src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
</body>
</html>